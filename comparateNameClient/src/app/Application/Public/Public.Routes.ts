import {Routes} from "@angular/router";
import {HomeComponent} from "./Module/home.component";
import {NameComponent} from "./Module/name.component";

export const PublicRoutes: Routes=[
  {path: 'home', component: HomeComponent},
  {path: 'names', component: NameComponent}
];
