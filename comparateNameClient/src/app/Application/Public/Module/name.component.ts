import {Component, OnInit} from "@angular/core";
import {NamesModel} from "../../../Domain/Models/NamesModel";
import {NameService} from "../../../Domain/Services/NameService";

@Component({
  providers: [NameService],
  templateUrl: 'name.component.html',
  selector: 'Name-app'
})

export class NameComponent implements OnInit {

  loadingTask = true;
  listNames: Array<NamesModel> = new Array<NamesModel>();
  currentPage = 1;
  itemsPerPage = 0;
  totalItems = 0;

  constructor(
    private nameService: NameService
  ) {

  }

  ngOnInit(): void {
    this.loadPageName();
  }

  changePage(e) {
    switch (e.target.innerText) {
      case 'chevron_right':
        if ((this.totalItems / this.itemsPerPage) > this.currentPage) {
          this.currentPage = this.currentPage + 1;
          this.loadPageName();
        }
        break;
      case 'chevron_left':
        if (this.currentPage > 1) {
          this.currentPage = this.currentPage - 1;
          this.loadPageName();
        }
        break;
      case 'first_page':
        if (this.currentPage != 1) {
          this.currentPage = 1;
          this.loadPageName();
        }
        break;
      case 'last_page':
        if ((this.totalItems / this.itemsPerPage) != this.currentPage) {
          let page = this.totalItems / this.itemsPerPage > 1 ? (this.totalItems % this.itemsPerPage) + 1 : this.totalItems % this.itemsPerPage;
          this.currentPage = page;
          this.loadPageName();
        }
        break;
      default:
        if (this.currentPage != e.target.innerText) {
          this.currentPage = e.target.innerText;
          this.loadPageName();
        }
        break;
    }
  }

  keyfilter(eve){
    let name = eve.target.value;
    if(name.length>0){
      this.currentPage = 0;
      this.loadPageName(name);
    }
  }
  loadPageName(name?: string) {
    this.nameService.GetNameList(this.currentPage,name).then(resp => {
      this.listNames = resp.Data.data;
      this.totalItems = resp.Data.total;
      this.currentPage = resp.Data.current_page;
      this.itemsPerPage = resp.Data.per_page;
      this.loadingTask = false;

    })
  }
}
