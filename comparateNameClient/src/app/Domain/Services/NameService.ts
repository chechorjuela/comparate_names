import {NamesModel} from "../Models/NamesModel";
import {Injectable} from "@angular/core";
import {INameRepository} from "../../Infrastructure/Repositories/Name.repository";
import {ResponseApiModel} from "../Models/ResponseApiModel";
import {ResponseData} from "../Models/ResponseData";

@Injectable()
export abstract class INameService {
  abstract GetNameList(currentPage: number, name: string): Promise<ResponseApiModel<ResponseData<NamesModel>>>;
}

@Injectable()
export class NameService implements INameService {
  constructor(public nameRepository: INameRepository) {

  }

  GetNameList(currentpage: number, name?: string): Promise<ResponseApiModel<ResponseData<NamesModel>>> {
    return this.nameRepository.getListNameAll(currentpage,name)
      .then(resp => {
        console.info(resp);
        return resp
      })
  }
}
