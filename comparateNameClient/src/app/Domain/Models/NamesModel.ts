export class NamesModel {
  Id: number;
  Departamento: string;
  Localidad: string;
  Municipio: string;
  Nombre: Date;
  Years_activo: string;
  Tipo_persona: number;
  Tipo_cargo: string;
  Porcentaje: string;
}
