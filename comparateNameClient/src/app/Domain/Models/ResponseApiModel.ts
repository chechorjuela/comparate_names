export class ResponseApiModel<T> {
  status:number;
  Header:ReponseApiHeaderModel;
  Data:T;
}

export class ReponseApiHeaderModel {
  status:string;
  message:string;
}
