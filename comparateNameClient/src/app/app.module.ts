import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './Application/Public/Module/app.component';

import {MaterializeComponentModule} from './Domain/Helpers/Modules/MaterializeComponentModule';
import {HomeComponent} from "./Application/Public/Module/home.component";
import {NameComponent} from "./Application/Public/Module/name.component";
import {AppSettings} from "./AppSettings";
import {HttpClientModule} from "@angular/common/http";
import {INameRepository, NameRepository} from "./Infrastructure/Repositories/Name.repository";
import {INameService, NameService} from "./Domain/Services/NameService";


@NgModule({
  declarations: [
    AppComponent,
    NameComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MaterializeComponentModule.forRoot(),
  ],
  providers: [
    AppSettings,
    {provide: INameRepository, useClass: NameRepository},
    {provide: INameService, useClass: NameService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
