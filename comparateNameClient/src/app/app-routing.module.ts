import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PublicRoutes} from "./Application/Public/Public.Routes";

const routes: Routes = [
  ...PublicRoutes
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
