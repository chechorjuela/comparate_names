export class AppSettings {
  public urlApi: string = "http://comparatenames.dev.com/api";
  public urls = {
    urlNames: '/user',
  };
  public statusResponse = {
    ok: 'success',
    fail: 'failed'
  };
}
