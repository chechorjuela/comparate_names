import {Injectable} from "@angular/core";
import {ResponseApiModel} from "../../Domain/Models/ResponseApiModel";
import {NamesModel} from "../../Domain/Models/NamesModel";
import {ResponseData} from "../../Domain/Models/ResponseData";
import {promise} from "selenium-webdriver";
import {AppSettings} from "../../AppSettings";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export abstract class INameRepository {
  abstract getListNameAll(currentpage: number, name?: string): Promise<ResponseApiModel<ResponseData<NamesModel>>>;

}

@Injectable()
export class NameRepository implements INameRepository {

  private baseUrl = new AppSettings().urlApi;
  private nameUrl = new AppSettings().urls.urlNames;
  public headersBasic = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': null});

  constructor(
    private http: HttpClient
  ) {

  }

  getListNameAll(currentpage: number, name?: string): Promise<ResponseApiModel<ResponseData<NamesModel>>> {
    let headersoptions = new HttpHeaders({'Content-Type': 'application/json'});
    let nameFilter = '';
    if (currentpage > 1) {
      nameFilter += '?page=' + currentpage;
    }
    if (typeof name != 'undefined') {
      nameFilter += "?name=" + name;
    }
    // @ts-ignore
    return this.http.get<ResponseApiModel<ResponseData<NamesModel>>>(this.baseUrl + this.nameUrl + nameFilter, {headers: headersoptions})
      .toPromise()
      .then(response => {
        //console.info(response);
        return response;
      })
      .catch(error => {
        console.info(error);
      });
  }

}
