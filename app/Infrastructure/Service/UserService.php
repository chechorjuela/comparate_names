<?php


namespace App\Infrastructure\Service;


use App\Domain\Repository\IUserRepository;
use App\Infrastructure\Requests\UserRequest;
use Illuminate\Http\Request;

class UserService
{
    private $_IUserRepository;

    /**
     * UserService constructor.
     * @param IUserRepository $IUSerRepositoryInterface
     */
    public function __construct(IUserRepository $IUSerRepositoryInterface)
    {
        $this->_IUserRepository = $IUSerRepositoryInterface;
    }

    public function get(Request $request)
    {
        return $this->_IUserRepository->all($request);
    }

    public function create(UserRequest $user)
    {
        return $this->_IUserRepository->store($user);
    }

    public function update(int $id, UserRequest $user)
    {
        return $this->_IUserRepository->update($id,$user);
    }

    public function delete(int $id)
    {
        return $this->_IUserRepository->delete($id);
    }
}
