<?php


namespace App\Infrastructure\Service;


use App\Infrastructure\Requests\UserRequest;
use Illuminate\Http\Request;

interface IUserServices
{
    public function get(Request $request);

    public function create(UserRequest $user);

    public function update(int $id,UserRequest $user);

    public function delete(int $id);
}
