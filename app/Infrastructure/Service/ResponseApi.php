<?php


namespace App\Infrastructure\Service;


class ResponseApi
{
    private $response = ['status' => 'fail', 'Data' => [], 'message' => ''];

    public function returnResponse($data, $code, $message = null)
    {
        $this->response['status'] = 'success';
        $this->response['Data'] = $data;
        if (isset($message)):
            $this->response['message'] = $message;
        endif;
        return response()->json($this->response, $code);
    }
}
