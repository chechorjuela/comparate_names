<?php


namespace App\Infrastructure\Http\Controllers\Api;


use App\Infrastructure\Http\Controllers\Controller;
use App\Infrastructure\Service\ResponseApi;
use App\Infrastructure\Service\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{

    private $_responseApi;

    private $_userService;

    public function __construct(UserService $userService, ResponseApi $responseApi)
    {
        $this->_responseApi = $responseApi;
        $this->_userService = $userService;
    }

    public function index(Request $request)
    {
        try {
            return $this->_responseApi->returnResponse($this->_userService->get($request), 200);
        } catch (\Exception $e) {
            return $this->resposeApi->returnResponse(null, 400, $e->getMessage(), "failed");
        }
    }

    public function store()
    {

    }

    public function show()
    {

    }

    public function update()
    {

    }

    public function destroy()
    {

    }
}
