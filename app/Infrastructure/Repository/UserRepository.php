<?php


namespace App\Infrastructure\Repository;


use App\Domain\Model\User;
use App\Domain\Repository\IUserRepository;
use Illuminate\Http\Request;

class UserRepository implements IUserRepository
{

    public function findById(int $id): ?User
    {
        // TODO: Implement findById() method.
    }

    public function search(): ?array
    {
        // TODO: Implement search() method.
    }

    public function update(int $id, Request $request): ?User
    {
        // TODO: Implement update() method.
    }

    public function store(Request $request): ?User
    {
        // TODO: Implement store() method.
    }

    public function all(Request $request): ?array
    {
        $name = $request->get('name');


        $users = User::filter($request->get('name'))->paginate(10)->toArray();
        foreach($users["data"] as &$user):
            similar_text($user["Nombre"], $name, $percent);
            $user["Porcentaje"] = $percent;

        endforeach;
        return $users;
    }

    public function delete(int $id): ?User
    {
        // TODO: Implement delete() method.
    }
}
