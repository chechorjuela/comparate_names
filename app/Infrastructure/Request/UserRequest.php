<?php

namespace App\Infrastructure\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'username' => 'required:min',
                    'firstname' => 'required:min:3',
                    'lastname' => 'required|min:3',
                    'password' => 'required|min:6',
                    'email' => 'email|required',
                    'role' => 'required',
                ];
                break;
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => 'El :attribute es requerido',
            'username.min' => 'El :attribute debe ser :value',
            'firstname.min' => 'EL :attribute debe ser :value',
            'firstname.required' => 'El :attribute  es requerido',
            'lastname.required' => 'El :attribute  es requerido',
            'lastname.min' => 'El :attribute debe contener :value',
            'password.required' => 'El :attribute  es requerido',
            'password.min' => 'El :attribute debe contener :value',
            'email.email' => 'El :attribute es incorrecto',
            'email.required' => 'El :attribute  es requerido',
            'role.required' => 'El :attribute es requerido',
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $json = [];
        $json["code"] = 200;
        $json["data"] = $validator->errors();
        throw new HttpResponseException(response()->json($json, 422));
    }
}
