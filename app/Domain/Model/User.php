<?php

namespace App\Domain\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Departamento', 'Municipio', 'Localidad','Nombre','Years_activo','Tipo_persona','Tipo_cargo'
    ];

    public function scopeFilter($query, $name)
    {
        if (trim($name) != "") {

            $query->where("Nombre", 'LIKE', "%$name%");

            //$query->where(\DB::raw("CONCAT(name,'',lastname)"),$name));
        }
    }
}
