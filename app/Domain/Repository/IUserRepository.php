<?php


namespace App\Domain\Repository;


use App\Domain\Model\User;
use Illuminate\Http\Request;

interface IUserRepository
{
    /**
     * @param int $id
     * @return User|null
     */
    public function findById(int $id): ?User;

    /**
     * @return array|null
     */
    public function search(): ?array;

    /**
     * @param int $id
     * @param Request $request
     * @return User|null
     */
    public function update(int $id, Request $request): ?User;

    /**
     * @param Request $request
     * @return User|null
     */
    public function store(Request $request): ?User;

    /**
     * @return array|null
     */
    public function all(Request $request): ?array;

    /**
     * @param int $id
     * @return User|null
     */
    public function delete(int $id): ?User;
}
